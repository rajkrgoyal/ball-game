# frozen_string_literal: true

# Calculate score for the player and find winner for 10-pin bowling
class ScoresController < ApplicationController
  def create
    status = status_for_scores

    render(status: status) && return if status != 200
    # @scores is in descending order. First value is highest
    # Finding winners, in case of there are multiple winners
    winners = @scores.select { |_k, v| v == @scores.first[1] }

    # Concatinating winners if there are multiple winners
    winners = winners.collect(&:first).join(', ')

    render json: {
      scores: @scores.to_h, winner: winners
    }
  end

  private

  def score_params
    params.require(:score).permit!
  end

  def status_for_scores
    @points = score_params

    @scores = []

    # Input should have minimum 1 and maximum 10 players
    return 422 if @points.keys.blank? || @points.keys.size > player_limit

    players_scores
  end

  def players_scores
    @points.each do |player, point|
      last_frame_index = point.size - 1

      status = validate_data(player, point)
      return status if status

      # declare variable for scope out of loop
      score = 0

      point.each_with_index do |frame, index|
        status = validate_frame(frame, index, last_frame_index)

        return status if status

        score = calculate_score(score, index, frame, point)
      end
      # Improve sorting
      # Avoid use of #sort_by to sort players by their score
      # Define new method for sorting
      sort_players(player, score)
    end
    200 # return 200 status
  end

  def validate_data(player, point)
    return 400 if bad_request?(player, point)

    # Each player should play same no of frames
    # Compare each player's frames with No of frames played by first player
    # According to game format, player should play max 10 times
    # 422 if @points.values[0].size != point.size || point.size > max_frames
  end

  # Check player name and input data format
  # Player name and scores can't be nil, empty / blank
  def bad_request?(player, point)
    (!point.is_a?(Array) || !player.is_a?(String) ||
      player.blank? || point.any?(&:blank?))
  end

  def validate_frame(frame, index, last_frame_index)
    return 400 if bad_frame?(frame)
    return 422 if unprocessable?(index, frame, last_frame_index)
  end

  ## input must be array and
  ## check all positive values in array
  def bad_frame?(frame)
    if !frame.is_a?(Array) || frame.any? { |i| i.is_a?(String) } ||
       frame.any?(&:negative?)
      return true
    end

    false
  end

  def unprocessable?(index, frame, last_frame_index)
    total = frame.sum
    last_frame_conditions(frame, total) if index == last_frame_index
    initial_frames_conditions(frame, total) if index < last_frame_index
  end

  # These situations are for last frame
  # For last 10th frame
  # [0, 10] - valid
  # [0, 10, 10] - valid
  # [10, 10] - invalid
  # [10, 10, 10] - valid
  # [10, 10, 0] - valid
  # [10, 0, 0] - valid
  # return true in case of invalid
  def last_frame_conditions(frame, total)
    ((frame[0] + frame[1]) >= max_score && (frame.size > 3 ||
      total > (max_score * 3))) ||
      ((frame[0] + frame[1]) < max_score && frame.size > 2)
  end

  # For 1 to 9th frames
  # [10, 0] - invalid
  # [0, 10] is valid
  # sum greater than 10 invalid
  def initial_frames_conditions(frame, total)
    (total > max_score || (frame[0] == max_score && frame.size > 1) ||
      (frame[0] < max_score && frame.size > 2))
  end

  def calculate_score(score, index, frame, point)
    score += frame.sum
    if index < (max_frames - 1) && frame.sum == max_score
      score += bonus(point, frame, index)
    end
    score
  end

  def bonus(point, frame, index)
    if frame[0] == max_score && point[index + 1].present?
      strike_bonus(point, index)
    elsif point[index + 1].present?
      point[index + 1][0]
    else
      0
    end
  end

  def strike_bonus(point, index)
    frame1 = point[index + 1]
    frame2 = point[index + 2]
    bonus = frame1[0]
    if frame1[1].present?
      bonus += frame1[1]
    elsif frame2.present?
      bonus += frame2[0]
    end
    bonus
  end

  # sort players by their scores in descending order
  def sort_players(player, score)
    players_count = @scores.size
    return @scores << [player, score] if @scores.empty?

    @scores.size.times do |i|
      # First condition
      # if new score is greater, then insert new player and score in array
      # Or condition
      # If score is same, compare player name in alphabatical order
      # Insert new player and score according to alphabatical order
      insert_player(player, score, i)
      # return in case of new player inserted in sorted array
      return @scores if players_count < @scores.size
    end

    @scores << [player, score]
  end

  def insert_player(player, score, index)
    if @scores[index][1] < score ||
       (@scores[index][1] == score && @scores[index][0] < player)
      @scores.insert(index, [player, score])
    elsif @scores[index][1] == score
      @scores.insert(index + 1, [player, score])
    end
  end

  # Maximum no of players can be played
  def player_limit
    10
  end

  # No of frames played by a player
  def max_frames
    10
  end

  # total score with in a frame
  def max_score
    10
  end
end
